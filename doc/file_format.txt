Python Sudoku File Format


Python Sudoku works with files that contains at least 16 numbers greater or
equal to 0. Zeros mean positions with not value. Numbers can use letters
instead of numbers greater than 9: A = 10, B = 11, etc.
The file can contain lines starting with #, this lines are comments. A
special case of comment is the board size definition.
There are two types of formats, the old and the new format. The new format is
the format in which Python Sudoku create the files.

Old Format:
If the sudoku is a regular one (ie, regions of XxY cells with X = Y, for
example 3x3), the board size definition is optional:
0 5 0  0 0 0  0 0 0
0 0 0  0 4 0  0 0 3
0 1 0  0 9 2  6 0 0

0 0 0  0 2 4  0 8 0
0 0 7  0 0 0  0 6 0
4 0 0  8 1 0  7 0 0

0 0 1  4 0 7  0 5 6
0 0 5  0 8 0  0 9 4
0 0 0  0 0 0  8 0 0

New Format:
If the sudoku is not regular (ie, regions of XxY whit X != Y, for example
3x5), the board size definition is required:
# boardsize 3 x 5
  0  0  6    0  E  4    0  9  0    0  0  B    0  0  0
  0  0  0    0  6  0    0  0  0    8  0  F    B  3  E
  4  D  0    B  0  5    0  0  0    0  2  0    0  0  0
  2  0  B    1  0  0    0  6  0    0  0  0    4  0  9
  9  0  0    0  0  0    0  0  7    0  0  A    0  0  0

  0  9  0    5  0  7    0  0  0    0  0  1    0  4  0
  F  0  0    0  0  0    0  4  0    2  6  0    0  7  0
  7  0  0    0  A  E    0  2  0    0  0  0    6  0  B
  0  5  0    0  B  0    0  A  C    0  0  0    2  0  0
  0  0  E    9  C  0    D  0  0    0  0  0    1  0  8

  C  0  3    0  0  0    5  0  0    0  7  4    0  0  D
  0  B  0    2  0  0    0  0  3    0  9  0    0  0  1
  A  1  5    0  3  0    C  0  0    0  0  D    0  0  0
  0  0  0    0  D  0    0  B  0    0  8  0    C  0  4
  0  0  7    0  0  0    0  0  0    F  0  0    8  A  0
